package labradoodle

import "testing"

func TestSpeak(t *testing.T) {
	d := &Dog{
		Name: "George",
	}

	if d.Speak() != "I'm George" {
		t.Errorf(`expected="I'm George", got="%s"`, d.Speak())
	}
}
