package labradoodle

type Dog struct {
	Name string
}

func (d *Dog) Speak() string {
	return "I'm " + d.Name
}
